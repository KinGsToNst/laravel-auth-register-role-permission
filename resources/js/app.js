require('./bootstrap');

$(document).ready(function () {
    console.log('jQuery works!');
});
$( function() {
    /* $( "#button" ).on( "click", function() {
         $( "#effect" ).toggleClass( "newClass", 1000 );
     });*/
    $('.toggle-class').on("change",function() {
        var status = $(this).prop('checked') == true ? 1 : 0;
        var user_id = $(this).data('id');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/changeStatus',
            data: {'status': status, 'user_id': user_id},
            success: function(data){
                console.log(data.success)
            }
        });
    });

} );

