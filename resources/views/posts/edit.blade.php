@extends('layouts.app')
@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header">Create post
                <span class="float-right">
                    <a class="btn btn-primary" href="{{ route('posts.index') }}">Статьи</a>
                </span>
            </div>
            <div class="card-body">
                {!! Form::model($post, ['route' => ['posts.update', $post->id], 'method'=>'PATCH']) !!}
                    <div class="form-group">
                        <strong>Название:</strong>
                        {!! Form::text('title', null, array('placeholder' => 'Название','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Текст:</strong>
                        {!! Form::textarea('body', null, array('placeholder' => 'Текст','class' => 'form-control')) !!}
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
