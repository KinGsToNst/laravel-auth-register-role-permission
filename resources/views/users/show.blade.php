@extends('layouts.app')
@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
        @endif
        <div class="card">
            <div class="card-header">User
                @can('role-create')
                    <span class="float-right">
                        <a class="btn btn-primary" href="{{ route('users.index') }}">Back</a>
                    </span>
                @endcan
            </div>
            <div class="card-body">
                <div class="lead">
                    <strong>Имя:</strong>
                    {{ $user->name }}
                </div>
                <div class="lead">
                    <strong>Email:</strong>
                    {{ $user->email }}
                </div>
                <div class="lead">
                    <strong>Image:</strong>
                    <img src="/image/avatars/{{ $user->image }}" width="300px">
                </div>
                <div class="lead">
                    <strong>Пароль:</strong>
                    ********
                </div>
                <div class="lead">
                    <strong>Роль:</strong>
                    {{ $user->role}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
