@extends('layouts.app')
@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header">Редактировать Пользователя
                <span class="float-right">
                    <a class="btn btn-primary" href="{{ route('users.index') }}">Пользователи</a>
                </span>
            </div>

            <div class="card-body">

                {!! Form::model($user, ['route' => ['users.update', $user->id], 'method'=>'PATCH','enctype'=>'multipart/form-data']) !!}
                    <div class="form-group">
                        <strong>Имя:</strong>
                        {!! Form::text('name', null, array('placeholder' => 'Имя','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Email:</strong>
                        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                    </div>

                <div class="form-group">
                    <strong>Image:</strong>
                    <input type="file" name="image" class="form-control" placeholder="image">
                    <img src="/image/avatars/{{ $user->image }}" width="300px">
                </div>
                    <div class="form-group">
                        <strong>Password:</strong>
                        {!! Form::password('password', array('placeholder' => 'Пароль','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Confirm Password:</strong>
                        {!! Form::password('password_confirmation', array('placeholder' => 'Повторите Пароль','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Role:</strong>
{{--                        {!! Form::select('roles[]', $roles,  $userRole, array('class' => 'form-control','multiple')) !!}--}}


                       <select name="roles[]"  class="form-control" multiple>
                            @foreach($roles as $userRole)
                            <option value="{{$userRole}}"  @if($userRole=='biblio') disabled @endif required>{{$userRole}}</option>
                            @endforeach
                        </select>

                    </div>
                    <button type="submit" class="btn btn-primary">Редактировать</button>
                {!! Form::close() !!}
            </div>
            <script>
                $('#delete-avatar').on('click', function(evt) {
                    console.log(11);
                    evt.preventDefault();
                    var $image = $('#avatar-container').find('img');
                    var $topImage = $('#top_avatar');
                    var $trashIcon = $(this);
                    var defaultImage = APP_URL + '/image/avatars/default.png';

                    //Get user's ID
                    var id = $(this).data('uid');
                    var fileName = $image.attr('src').split('/').reverse()[0];

                    if (confirm('Delete the avatar?')) {
                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                        $.ajax({
                            url: APP_URL + `/dashboard/profile/deleteavatar/${id}/${fileName}`,
                            method: 'POST',
                            data: {
                                id: id,
                                fileName: fileName,
                                _token: CSRF_TOKEN,
                            },
                            success: function() {
                                $image.attr('src', defaultImage);
                                $topImage.attr('src', defaultImage);
                                $trashIcon.remove();
                            }
                        });
                    }
                });
            </script>
        </div>
    </div>
</div>
@endsection
