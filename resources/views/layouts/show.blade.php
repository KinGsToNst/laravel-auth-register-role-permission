@extends('layouts.app')
@section('content')
<div class="container">
    <div class="justify-content-center">
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
        @endif

        <div class="card">
            <div class="card-header">Пользователь
                @can('role-create')
                    <span class="float-right">
                        <a class="btn btn-primary" href="{{ route('users.index') }}">Назад</a>
                    </span>
                @endcan
            </div>

            <div class="card-body">
                <div class="lead">
                    <strong>Имя:</strong>

                </div>
                <div class="lead">
                    <strong>Email:</strong>
                    {{ $user->email }}
                </div>
                <div class="lead">
                    <strong>Пароль:</strong>
                    ********
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
