@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Профиль пользователя') }}</div>

                <div class="card-body">
                    <div class="lead">
                        <strong>ID:</strong>
                        {{ Auth::user()->id}}
                    </div>
                    <div class="lead">
                        <strong>Имя:</strong>
                        {{ Auth::user()->name }}
                    </div>
                    <div class="lead">
                        <strong>Email:</strong>
                        {{ Auth::user()->email }}

                    </div>


                    <div class="lead">
                        <strong>Пароль:</strong>
                        password:
                    </div>
                    @if(!empty(Auth::user()->getRoleNames()))
                        <strong>Роль:</strong>
                        @foreach(Auth::user()->getRoleNames() as $val)
                            {{ $val }}
                        @endforeach
                    @endif
                    <div class="lead">
                        <strong>фото профиля:</strong>
                        <img src="/image/{{Auth::user()->image}}" width="100px">
                    </div>

                    </br>
=====================Редактирование профиля пока не работает====================

                    {!! Form::model(Auth::user(), ['route' => ['users.update', Auth::user()->id], 'method'=>'PATCH']) !!}
                        {{ csrf_field() }}

                    <div class="form-group">
                        <strong>Имя:</strong>
                        {!! Form::text('name', null, array('placeholder' => 'Имя','class' => 'form-control')) !!}
                    </div>
{{--                    <div class="form-group">--}}
{{--                        <strong>Email:</strong>--}}
{{--                        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <strong>Пароль:</strong>--}}
{{--                        {!! Form::password('password', array('placeholder' => 'Пароль','class' => 'form-control')) !!}--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <strong>Подтвердить Пароль:</strong>--}}
{{--                        {!! Form::password('password_confirmation', array('placeholder' => 'Подтвердить Пароль','class' => 'form-control')) !!}--}}
{{--                    </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <strong>Email:</strong>--}}
{{--                            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}--}}
{{--                        </div>--}}


                    <button type="submit" class="btn btn-primary">обновить</button>
                    {!! Form::close() !!}




                </div>
            </div>
        </div>
    </div>
</div>
@endsection
