<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\File;
use App\Models\User;

class UserProfileController extends Controller
{
    public function index(User $user)
    {
        return view('dashboard.userprofile',
            array('user' => Auth::user())
        );
    }

    public function update(Request $request)
    {




        if ($request->isMethod('GET')) {
            return redirect()->route('profile');
        }

        $user = Auth::user();



        $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:users,name,' . $user->id],
            'email' => 'required|email|unique:users,email,'.$user->id,
           // 'password' => 'confirmed',
        ]);




        $user->name = $request->get('name');
        $user->email = $request->get('email');
//        $current_user->first_name = $request->get('first_name');
//        $current_user->last_name = $request->get('last_name');
      //  $current_user->email = $request->get('email');
      //  $current_user->bio = $request->get('bio');


        // Upload avatar
       if (isset($request->image)) {
            $imageName = md5(time()) . $user->id . '.' . $request->image->extension();
            $request->image->move(public_path('image/avatars'), $imageName);
            $user->image = $imageName;
        }

        // Update user
        $user->update();
        return redirect('dashboard/profile')
            ->with('success', 'User data updated successfully');
    }


    public function deleteavatar($id, $fileName)
    {
        $user = Auth::user();
        $user->image = "default.png";
        $user->save();

        if (File::exists(public_path('image/avatars/' . $fileName))) {
            File::delete(public_path('image/avatars/' . $fileName));
        }
    }




}
