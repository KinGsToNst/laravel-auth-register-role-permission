<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
           Product::create([
                'name' => $faker->sentence,
                'slug' => $faker->unique()->slug,
                'description'=> $faker->paragraph,
                'price' =>$faker->numberBetween($min =50.2, $max = 60.2),
            ]);


          /*  DB::table('products')->insert([
                'name' => $faker->sentence,
                'slug' => $faker->unique()->slug,
                'description'=> $faker->paragraph,
                'price' =>$faker->numberBetween($min = 5000, $max = 6000),
            ]);*/
        }
    }
}
